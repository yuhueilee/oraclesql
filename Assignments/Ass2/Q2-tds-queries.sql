--****PLEASE ENTER YOUR DETAILS BELOW****
--Q2-tds-queries.sql
--Student ID: 29350336
--Student Name: Lee Yu Huei
--Tutorial No: 3

/* Comments for your marker:

*/

/*
2(i) Query 1
*/
--PLEASE PLACE REQUIRED SQL STATEMENT FOR THIS PART HERE
SELECT
    dem_points        AS "Demerit Point",
    dem_description   AS "Demerit Description"
FROM
    demerit
WHERE
    lower(dem_description) LIKE ( '%heavy%' )
    OR lower(dem_description) LIKE ( 'exceed%' )
ORDER BY
    dem_points,
    dem_description;

/*
2(ii) Query 2
*/
--PLEASE PLACE REQUIRED SQL STATEMENT FOR THIS PART HERE

SELECT
    veh_maincolor   AS "Main Colour",
    veh_vin         AS vin,
    lpad(EXTRACT(YEAR FROM veh_yrmanuf), 4, ' ') AS "Year Manufactured"
FROM
    vehicle
WHERE
    ( lower(veh_modname) LIKE ( '%range%rover%' )
      OR lower(veh_modname) LIKE ( '%range%rover%sport' ) )
    AND EXTRACT(YEAR FROM veh_yrmanuf) BETWEEN 2012 AND 2014
ORDER BY
    veh_yrmanuf DESC,
    veh_maincolor;

/*
2(iii) Query 3
*/
--PLEASE PLACE REQUIRED SQL STATEMENT FOR THIS PART HERE

SELECT
    d.lic_no   AS "Licence No.",
    lic_fname
    || ' '
    || lic_lname AS "Driver Fullname",
    lic_dob    AS dob,
    lic_street
    || ' '
    || lic_town
    || ' '
    || lic_postcode AS "Driver Address",
    to_char(sus_date, 'dd/Mon/yyyy') AS "Suspended On",
    to_char(sus_enddate, 'dd/Mon/yyyy') AS "Suspended Till"
FROM
    driver       d
    JOIN suspension   s
    ON d.lic_no = s.lic_no
WHERE
    months_between(sus_date, sysdate) <= 30
ORDER BY
    d.lic_no,
    sus_date DESC;

/*
2(iv) Query 4
*/
--PLEASE PLACE REQUIRED SQL STATEMENT FOR THIS PART HERE

SELECT
    d.dem_code          AS "Demerit Code",
    d.dem_description   AS "Demerit Description",
    COUNT(o.dem_code) AS "Total Offences (All Months)",
    (
        SELECT
            COUNT(o.dem_code)
        FROM
            offence o
        WHERE
            EXTRACT(MONTH FROM o.off_datetime) = 1
            AND o.dem_code = d.dem_code
    ) AS "Jan",
    (
        SELECT
            COUNT(o.dem_code)
        FROM
            offence o
        WHERE
            EXTRACT(MONTH FROM o.off_datetime) = 2
            AND o.dem_code = d.dem_code
    ) AS "Feb",
    (
        SELECT
            COUNT(o.dem_code)
        FROM
            offence o
        WHERE
            EXTRACT(MONTH FROM o.off_datetime) = 3
            AND o.dem_code = d.dem_code
    ) AS "Mar",
    (
        SELECT
            COUNT(o.dem_code)
        FROM
            offence o
        WHERE
            EXTRACT(MONTH FROM o.off_datetime) = 4
            AND o.dem_code = d.dem_code
    ) AS "Apr",
    (
        SELECT
            COUNT(o.dem_code)
        FROM
            offence o
        WHERE
            EXTRACT(MONTH FROM o.off_datetime) = 5
            AND o.dem_code = d.dem_code
    ) AS "May",
    (
        SELECT
            COUNT(o.dem_code)
        FROM
            offence o
        WHERE
            EXTRACT(MONTH FROM o.off_datetime) = 6
            AND o.dem_code = d.dem_code
    ) AS "Jun",
    (
        SELECT
            COUNT(o.dem_code)
        FROM
            offence o
        WHERE
            EXTRACT(MONTH FROM o.off_datetime) = 7
            AND o.dem_code = d.dem_code
    ) AS "Jul",
    (
        SELECT
            COUNT(o.dem_code)
        FROM
            offence o
        WHERE
            EXTRACT(MONTH FROM o.off_datetime) = 8
            AND o.dem_code = d.dem_code
    ) AS "Aug",
    (
        SELECT
            COUNT(o.dem_code)
        FROM
            offence o
        WHERE
            EXTRACT(MONTH FROM o.off_datetime) = 9
            AND o.dem_code = d.dem_code
    ) AS "Sep",
    (
        SELECT
            COUNT(o.dem_code)
        FROM
            offence o
        WHERE
            EXTRACT(MONTH FROM o.off_datetime) = 10
            AND o.dem_code = d.dem_code
    ) AS "Oct",
    (
        SELECT
            COUNT(o.dem_code)
        FROM
            offence o
        WHERE
            EXTRACT(MONTH FROM o.off_datetime) = 11
            AND o.dem_code = d.dem_code
    ) AS "Nov",
    (
        SELECT
            COUNT(o.dem_code)
        FROM
            offence o
        WHERE
            EXTRACT(MONTH FROM o.off_datetime) = 12
            AND o.dem_code = d.dem_code
    ) AS "Dec"
FROM
    offence   o
    RIGHT OUTER JOIN demerit   d
    ON o.dem_code = d.dem_code
GROUP BY
    d.dem_code,
    d.dem_description
ORDER BY
    "Total Offences (All Months)" DESC,
    d.dem_code;

/*
2(v) Query 5
*/
--PLEASE PLACE REQUIRED SQL STATEMENT FOR THIS PART HERE

SELECT
    v.veh_manufname AS "Manufacturer Name",
    COUNT(o.off_no) AS "Total No. of Offences"
FROM
    vehicle   v
    JOIN offence   o
    ON v.veh_vin = o.veh_vin
WHERE
    o.dem_code IN (
        SELECT
            dem_code
        FROM
            demerit
        WHERE
            dem_points >= 2
    )
GROUP BY
    v.veh_manufname
HAVING
    COUNT(o.off_no) IN (
        SELECT
            MAX(COUNT(o.off_no))
        FROM
            offence   o
            JOIN vehicle   v
            ON o.veh_vin = v.veh_vin
        WHERE
            o.dem_code IN (
                SELECT
                    dem_code
                FROM
                    demerit
                WHERE
                    dem_points >= 2
            )
        GROUP BY
            v.veh_manufname
    )
ORDER BY
    "Total No. of Offences" DESC,
    v.veh_manufname;

/*
2(vi) Query 6
*/
--PLEASE PLACE REQUIRED SQL STATEMENT FOR THIS PART HERE

SELECT DISTINCT
    o.lic_no         AS "Licence No.",
    d.lic_fname
    || ' '
    || d.lic_lname AS "Driver Name",
    ofc.officer_id   AS "Officer ID",
    ofc.officer_fname
    || ' '
    || ofc.officer_lname AS "Officer Name"
FROM
    driver    d
    JOIN offence   o
    ON d.lic_no = o.lic_no
    JOIN officer   ofc
    ON o.officer_id = ofc.officer_id
WHERE
    d.lic_lname = ofc.officer_lname
    AND o.dem_code IN (
        SELECT
            o.dem_code
        FROM
            offence o
        GROUP BY
            dem_code,
            o.lic_no
        HAVING
            COUNT(o.lic_no) > 1
    )
ORDER BY
    o.lic_no;

/*
2(vii) Query 7
*/
--PLEASE PLACE REQUIRED SQL STATEMENT FOR THIS PART HERE

SELECT
    d.dem_code          AS "Demerit Code",
    d.dem_description   AS "Demerit Description",
    o.lic_no            AS "Licence No.",
    dr.lic_fname
    || ' '
    || dr.lic_lname AS "Driver Fullname",
    COUNT(o.lic_no) AS "Total Times Booked"
FROM
    demerit   d
    JOIN offence   o
    ON d.dem_code = o.dem_code
    JOIN driver    dr
    ON o.lic_no = dr.lic_no
GROUP BY
    d.dem_code,
    d.dem_description,
    o.lic_no,
    dr.lic_fname,
    dr.lic_lname
HAVING
    COUNT(o.lic_no) = (
        SELECT
            MAX("Total Times Booked")
        FROM
            (
                SELECT
                    COUNT(o.lic_no) AS "Total Times Booked"
                FROM
                    offence o
                WHERE
                    o.dem_code = d.dem_code
                GROUP BY
                    o.dem_code,
                    o.lic_no
            )
    )
ORDER BY
    d.dem_code,
    o.lic_no;

/*
2(viii) Query 8
*/
--PLEASE PLACE REQUIRED SQL STATEMENT FOR THIS PART HERE

SELECT
    region,
    COUNT(veh_vin) AS "Total Vehicles Manufactured",
    lpad(to_char(round((COUNT(veh_vin) /(
        SELECT
            COUNT(veh_vin)
        FROM
            vehicle
    )) * 100, 2), '90.00')
         || '%', 30, ' ') AS "Percentage of Vehicles Manufactured"
FROM
    (
        SELECT
            v.veh_vin,
            CASE
                WHEN upper(substr(v.veh_vin, 1, 1)) BETWEEN 'A' AND 'C' THEN
                    'Africa'
                WHEN upper(substr(v.veh_vin, 1, 1)) BETWEEN 'J' AND 'R' THEN
                    'Asia'
                WHEN upper(substr(v.veh_vin, 1, 1)) BETWEEN 'S' AND 'Z' THEN
                    'Europe'
                WHEN upper(substr(v.veh_vin, 1, 1)) BETWEEN '1' AND '5' THEN
                    'North'
                    || ' '
                    || 'America'
                WHEN upper(substr(v.veh_vin, 1, 1)) BETWEEN '6' AND '7' THEN
                    'Oceania'
                WHEN upper(substr(v.veh_vin, 1, 1)) BETWEEN '8' AND '9' THEN
                    'South'
                    || ' '
                    || 'America'
                ELSE
                    'Unknown'
            END AS region
        FROM
            vehicle v
    )
GROUP BY
    region
UNION
SELECT
    lpad(region, 15, ' '),
    COUNT(veh_vin) AS "Total Vehicles Manufactured",
    (
        SELECT
            lpad((to_char(SUM(round((COUNT(veh_vin) /(
                SELECT
                    COUNT(veh_vin)
                FROM
                    vehicle
            )) * 100, 2)), '90.99')
                  || ''
                  || '%'), 30, ' ') AS "Percentage of Vehicles Manufactured"
        FROM
            vehicle v
        GROUP BY
            CASE
                WHEN upper(substr(v.veh_vin, 1, 1)) BETWEEN 'A' AND 'C' THEN
                    'Africa'
                WHEN upper(substr(v.veh_vin, 1, 1)) BETWEEN 'J' AND 'R' THEN
                    'Asia'
                WHEN upper(substr(v.veh_vin, 1, 1)) BETWEEN 'S' AND 'Z' THEN
                    'Europe'
                WHEN upper(substr(v.veh_vin, 1, 1)) BETWEEN '1' AND '5' THEN
                    'North'
                    || ' '
                    || 'America'
                WHEN upper(substr(v.veh_vin, 1, 1)) BETWEEN '6' AND '7' THEN
                    'Oceania'
                WHEN upper(substr(v.veh_vin, 1, 1)) BETWEEN '8' AND '9' THEN
                    'South'
                    || ' '
                    || 'America'
                ELSE
                    'Unknown'
            END
    )
FROM
    (
        SELECT
            veh_vin,
            CASE
                WHEN substr(veh_vin, 1, 1) BETWEEN '1' AND 'Z' THEN
                    'TOTAL'
            END AS region
        FROM
            vehicle
    )
GROUP BY
    region
ORDER BY
    "Total Vehicles Manufactured",
    region;