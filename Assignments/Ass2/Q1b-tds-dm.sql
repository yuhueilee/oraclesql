--****PLEASE ENTER YOUR DETAILS BELOW****
--Q1b-tds-dm.sql
--Student ID: 29350336
--Student Name: Lee Yu Huei
--Tutorial No: 3
SET SERVEROUTPUT ON;

/* Comments for your marker:



*/

DROP SEQUENCE offence_seq;

/*
1b(i) Create a sequence 
*/
--PLEASE PLACE REQUIRED SQL STATEMENT(S) FOR THIS PART HERE

CREATE SEQUENCE offence_seq INCREMENT BY 1 START WITH 100;



/*
1b(ii) Take the necessary steps in the database to record data.
*/
--PLEASE PLACE REQUIRED SQL STATEMENT(S) FOR THIS PART HERE

INSERT INTO offence VALUES (
    offence_seq.NEXTVAL,
    TO_DATE('10/Aug/2019 08:04 AM', 'dd/Mon/yyyy HH:MI AM'),
    '679 Chapel Street South Yarra 3141',
    (
        SELECT
            dem_code
        FROM
            demerit
        WHERE
            dem_description = 'Blood alcohol charge'
    ),
    10000011,
    '100389',
    'JYA3HHE05RA070562'
);

COMMIT;

INSERT INTO offence VALUES (
    offence_seq.NEXTVAL,
    TO_DATE('16/Oct/2019 09:00 PM', 'dd/Mon/yyyy HH:MI AM'),
    '392 Lonsdale Street Melbourne 3000',
    (
        SELECT
            dem_code
        FROM
            demerit
        WHERE
            dem_description = 'Level crossing offence'
    ),
    10000015,
    '100389',
    'JYA3HHE05RA070562'
);

COMMIT;

INSERT INTO offence VALUES (
    offence_seq.NEXTVAL,
    TO_DATE('7/Jan/2020 07:07 AM', 'dd/Mon/yyyy HH:MI AM'),
    '44 Labertouche Road Longwarry North 3816',
    (
        SELECT
            dem_code
        FROM
            demerit
        WHERE
            dem_description = 'Exceeding the speed limit by 25 km/h or more'
    ),
    10000015,
    '100389',
    'JYA3HHE05RA070562'
);

COMMIT;



/*
1b(iii) Take the necessary steps in the database to record changes. 
*/
--PLEASE PLACE REQUIRED SQL STATEMENT(S) FOR THIS PART HERE

UPDATE offence
SET
    dem_code = (
        SELECT
            dem_code
        FROM
            demerit
        WHERE
            dem_description = 'Exceeding the speed limit by 10 km/h or more but less than 25 km/h'
    )
WHERE
    lic_no = '100389' AND
    to_char(off_datetime, 'dd/Mon/yyyy HH:MI AM') = '07/Jan/2020 07:07 AM';

COMMIT;