--****PLEASE ENTER YOUR DETAILS BELOW****
--Q3-tds-mods.sql
--Student ID: 29350336
--Student Name: Lee Yu Huei
--Tutorial No: 3

/* Comments for your marker:

*/
DROP TABLE revocation CASCADE CONSTRAINTS PURGE;

DROP TABLE reason CASCADE CONSTRAINTS PURGE;

/*
3(i) Changes to live database 1
*/
--PLEASE PLACE REQUIRED SQL STATEMENTS FOR THIS PART HERE

ALTER TABLE officer ADD officer_booking NUMERIC(5, 0) DEFAULT 0;

COMMENT ON COLUMN officer.officer_booking IS
    'No of times the officer booked a driver';

UPDATE officer
SET
    officer_booking = (
        SELECT
            COUNT(offence.officer_id)
        FROM
            offence
        WHERE
            offence.officer_id = officer.officer_id
    );

COMMIT;

/*
3(ii) Changes to live database 2
*/
--PLEASE PLACE REQUIRED SQL STATEMENTS FOR THIS PART HERE

ALTER TABLE offence ADD (
    off_revoked CHAR(3) DEFAULT 'NO' NOT NULL
);

COMMENT ON COLUMN offence.off_revoked IS
    'offence revoked or not';

ALTER TABLE offence
    ADD CONSTRAINT ck_off_revoked CHECK ( off_revoked IN (
        'YES',
        'NO'
    ) );

COMMIT;

CREATE TABLE reason (
    reason_code          CHAR(3) NOT NULL,
    reason_description   VARCHAR(100) NOT NULL
);

COMMENT ON COLUMN reason.reason_code IS
    'reason code';

COMMENT ON COLUMN reason.reason_description IS
    'reason description';

ALTER TABLE reason ADD CONSTRAINT pk_reason PRIMARY KEY ( reason_code );

ALTER TABLE reason
    ADD CONSTRAINT ck_reason_code CHECK ( reason_code IN (
        'FOS',
        'FEU',
        'DOU',
        'COH',
        'EIP'
    ) );

INSERT INTO reason VALUES (
    'FOS',
    'First offence exceeding the speed limit by less than 10km/h'
);

INSERT INTO reason VALUES (
    'FEU',
    'Faulty equipment used'
);

INSERT INTO reason VALUES (
    'DOU',
    'Driver objection upheld'
);

INSERT INTO reason VALUES (
    'COH',
    'Court hearing'
);

INSERT INTO reason VALUES (
    'EIP',
    'Error in proceedings'
);

COMMIT;

CREATE TABLE revocation (
    rev_id         NUMBER(5, 0) NOT NULL,
    rev_datetime   DATE NOT NULL,
    off_no         NUMBER(8, 0) NOT NULL,
    officer_id     NUMBER(8, 0) NOT NULL,
    reason_code    CHAR(3)
);

COMMENT ON COLUMN revocation.rev_id IS
    'revocation id';

COMMENT ON COLUMN revocation.rev_datetime IS
    'revocation date time';

COMMENT ON COLUMN revocation.off_no IS
    'offence number';

COMMENT ON COLUMN revocation.officer_id IS
    'officer id who revoked offence';

COMMENT ON COLUMN revocation.reason_code IS
    'revocation reason code';

ALTER TABLE revocation ADD CONSTRAINT pk_revocation PRIMARY KEY ( rev_id );

ALTER TABLE revocation ADD CONSTRAINT uk_revocation UNIQUE ( off_no );

ALTER TABLE revocation
    ADD CONSTRAINT fk_rev_reason FOREIGN KEY ( reason_code )
        REFERENCES reason ( reason_code );

ALTER TABLE revocation
    ADD CONSTRAINT fk_rev_offence FOREIGN KEY ( off_no )
        REFERENCES offence ( off_no );

ALTER TABLE revocation
    ADD CONSTRAINT fk_rev_officer FOREIGN KEY ( officer_id )
        REFERENCES officer ( officer_id );

COMMIT;