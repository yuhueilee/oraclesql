-- Vehicle VIN
select veh_vin, veh_type, to_char(veh_yrmanuf, 'dd/mon/yyyy') as year_of_manufacture
from vehicle
where extract(year from veh_yrmanuf) < '2019'
ORDER BY veh_vin, veh_type;

-- Demerit MB
select *
from demerit
where lower(dem_description) NOT LIKE ('%driv%') OR lower(dem_description) LIKE ('%helmet%');

-- Demerit Car
select *
from demerit
where lower(dem_description) LIKE ('%driv%');

-- License No
select lic_no, lic_street || ' ' || lic_town || ' ' || lic_postcode as address
from driver
order by lic_postcode;