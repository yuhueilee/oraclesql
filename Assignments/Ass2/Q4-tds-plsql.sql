--****PLEASE ENTER YOUR DETAILS BELOW****
--Q4-tds-plsql.sql
--Student ID: 29350336
--Student Name: Lee Yu Huei
--Tutorial No: 3
/* Comments for your marker:

*/
DROP TRIGGER officer_booking_upd;

DROP TRIGGER maintain_driver_name;

DROP TRIGGER maintain_officer_name;

DROP TRIGGER driver_lic_upd;

DROP TABLE lic_exp_history CASCADE CONSTRAINTS PURGE;

DROP SEQUENCE lic_exp_history_seq;

/* (i) Write a trigger which will, from this point forward, automatically maintain the total number of times each police officer has booked a driver for a traffic offence attribute you added in Task 3. 
*/
/*Please copy your trigger code and any other necessary SQL statements after this line*/

CREATE OR REPLACE TRIGGER officer_booking_upd AFTER
    INSERT OR UPDATE OF officer_id ON offence
    FOR EACH ROW
BEGIN 
-- for insert, update the total number of booking for the new officer id
    IF inserting THEN
        UPDATE officer
        SET
            officer_booking = officer_booking + 1
        WHERE
            officer_id = :new.officer_id;

        dbms_output.put_line('Officer '
                             || :new.officer_id
                             || ' total number of booking has been incremented by 1'
                             );
    ELSE
-- for updating, increment the total number of booking for the new officer id and decrement the old officer id
        IF updating THEN
            UPDATE officer
            SET
                officer_booking = officer_booking + 1
            WHERE
                officer_id = :new.officer_id;

            UPDATE officer
            SET
                officer_booking = officer_booking - 1
            WHERE
                officer_id = :old.officer_id;

            dbms_output.put_line('The old officer '
                                 || :old.officer_id
                                 || ' has been replaced by the new officer '
                                 || :new.officer_id);

        END IF;
    END IF;
END;
/
--- Insert offence ---

SELECT
    *
FROM
    offence;

SELECT
    *
FROM
    officer;

INSERT INTO offence VALUES (
    offence_seq.NEXTVAL,
    TO_DATE('15/May/2019 02:50 PM', 'dd/Mon/yyyy HH:MI AM'),
    '106 Moriah Street Clayton 3168',
    100,
    10000001,
    '100133',
    'WVWDA7AJ4EW010470',
    'NO'
);

SELECT
    *
FROM
    offence;

SELECT
    *
FROM
    officer;
    
--- Update offence ---

SELECT
    *
FROM
    offence;

SELECT
    *
FROM
    officer;

UPDATE offence
SET
    officer_id = 10000002
WHERE
    off_no = (
        SELECT
            off_no
        FROM
            offence
        WHERE
            to_char(off_datetime, 'dd/Mon/yyyy') = '15/May/2019'
    );

SELECT
    *
FROM
    offence;

SELECT
    *
FROM
    officer;

ROLLBACK;
/* (ii) Write a trigger which will, from this point forward, make sure that driver and police have at least one name before their data is added into the database. The trigger must prevent the insertion if both names are nulls. 
*/
/*Please copy your trigger code and any other necessary SQL statements after this line*/

CREATE OR REPLACE TRIGGER maintain_driver_name BEFORE
    INSERT OR UPDATE OF lic_fname, lic_lname ON driver
    FOR EACH ROW
BEGIN
    IF :new.lic_fname IS NULL AND :new.lic_lname IS NULL THEN
        raise_application_error(-20001, 'Driver must have at least one first name or last name'
        );
    END IF;
END;
/

CREATE OR REPLACE TRIGGER maintain_officer_name BEFORE
    INSERT OR UPDATE OF officer_fname, officer_lname ON officer
    FOR EACH ROW
BEGIN
    IF :new.officer_fname IS NULL AND :new.officer_lname IS NULL THEN
        raise_application_error(-20001, 'Officer must have at least one first name or last name'
        );
    END IF;
END;
/

--- Insert driver ---

SELECT
    *
FROM
    driver;

INSERT INTO driver VALUES (
    '100400',
    'Wendy',
    NULL,
    '012345679',
    '100 Almond Alley',
    'Melbourne',
    '3000',
    TO_DATE('26-Jul-1951', 'DD-MON-YYYY'),
    TO_DATE('20-Feb-2021', 'DD-MON-YYYY')
);

INSERT INTO driver VALUES (
    '100401',
    NULL,
    'Lee',
    '012345678',
    '100 Almond Alley',
    'Melbourne',
    '3000',
    TO_DATE('26-Jul-1951', 'DD-MON-YYYY'),
    TO_DATE('20-Feb-2021', 'DD-MON-YYYY')
);

INSERT INTO driver VALUES (
    '100402',
    NULL,
    NULL,
    '012345670',
    '100 Almond Alley',
    'Melbourne',
    '3000',
    TO_DATE('26-Jul-1951', 'DD-MON-YYYY'),
    TO_DATE('20-Feb-2021', 'DD-MON-YYYY')
);

SELECT
    *
FROM
    driver;

--- Update driver ---

UPDATE driver
SET
    lic_fname = NULL,
    lic_lname = NULL
WHERE
    lic_no = '100399';

-- Insert officer ---

SELECT
    *
FROM
    officer;

INSERT INTO officer VALUES (
    10000022,
    'David',
    NULL,
    0
);

INSERT INTO officer VALUES (
    10000023,
    NULL,
    'John',
    0
);

INSERT INTO officer VALUES (
    10000024,
    NULL,
    NULL,
    0
);

SELECT
    *
FROM
    officer;

--- Update officer ---

UPDATE officer
SET
    officer_fname = NULL,
    officer_lname = NULL
WHERE
    officer_id = 10000021;

ROLLBACK;

/* (iii) The local government wants to maintain a history of all drivers’ license expiry dates. Write a trigger to record the current and new lic_expiry date of a driver’s license whenever there is a change in a driver’s license expiry date. The trigger must check if the new licence expiry date is at least 30 months (2.5 years) later than current license expiry date, otherwise it must prevent the change.
Hint: to carry out this task, you need to create another table where the history of all drivers’ license expiry dates is recorded. In the table, include the licence number, the current expiry date, the new expiry date and the date when the update is done.
*/
/*Please copy your trigger code and any other necessary SQL statements after this line*/

CREATE TABLE lic_exp_history (
    lic_exp_history_no    NUMBER(5, 0) NOT NULL,
    lic_no                CHAR(10) NOT NULL,
    lic_expiry            DATE NOT NULL,
    lic_expiry_new        DATE NOT NULL,
    lic_update_datetime   DATE NOT NULL
);

COMMENT ON COLUMN lic_exp_history.lic_no IS
    'license number';

COMMENT ON COLUMN lic_exp_history.lic_expiry IS
    'license expiry current date';

COMMENT ON COLUMN lic_exp_history.lic_expiry_new IS
    'license expiry new date';

COMMENT ON COLUMN lic_exp_history.lic_update_datetime IS
    'license expiry update date time';

ALTER TABLE lic_exp_history ADD CONSTRAINT pk_lic_exp_history PRIMARY KEY ( lic_exp_history_no
);

ALTER TABLE lic_exp_history ADD CONSTRAINT uk_lic_exp_history UNIQUE ( lic_no
,
                                                                          lic_update_datetime
                                                                          );

ALTER TABLE lic_exp_history
    ADD CONSTRAINT fk_lic_exp_history_driver FOREIGN KEY ( lic_no )
        REFERENCES driver ( lic_no );

CREATE SEQUENCE lic_exp_history_seq INCREMENT BY 1 START WITH 10001;

CREATE OR REPLACE TRIGGER driver_lic_upd BEFORE
    UPDATE ON driver
    FOR EACH ROW
BEGIN
    IF months_between(:new.lic_expiry, :old.lic_expiry) < 30 THEN
        raise_application_error(-20001, 'New license expiry date must be at least 30 months later than the current license expiry date.'
        );
    ELSE
        INSERT INTO lic_exp_history (
            lic_exp_history_no,
            lic_no,
            lic_expiry,
            lic_expiry_new,
            lic_update_datetime
        ) VALUES (
            lic_exp_history_seq.NEXTVAL,
            :new.lic_no,
            :old.lic_expiry,
            :new.lic_expiry,
            to_date(sysdate, 'dd/Mon/yyyy HH24:MI:SS')
        );

        dbms_output.put_line('Driver with license no '
                             || rtrim(:new.lic_no)
                             || ' has updated the license expiry date from '
                             || to_char(:old.lic_expiry, 'dd/Mon/yyyy')
                             || ' to '
                             || to_char(:new.lic_expiry, 'dd/Mon/yyyy'));

    END IF;
END;
/

--- Update driver ---
SELECT * FROM driver WHERE lic_no = '100399';
SELECT * FROM lic_exp_history;

UPDATE driver
    SET lic_expiry = TO_DATE('19-Feb-2032', 'DD-MON-YYYY')
    WHERE lic_no = '100399';

UPDATE driver
    SET lic_expiry = TO_DATE('20-Feb-2032', 'DD-MON-YYYY')
    WHERE lic_no = '100399';

SELECT * FROM driver WHERE lic_no = '100399';
SELECT * FROM lic_exp_history;


ROLLBACK;
