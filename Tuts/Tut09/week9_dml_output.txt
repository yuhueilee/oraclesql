SQL> -- 9.2.1
SQL> 
SQL> -- Task 1: Update the unit name of FIT9999 from 'FIT Last Unit' to 'place holder unit'.
SQL> UPDATE unit
  2  SET
  3      unit_name = 'place holder unit'
  4  WHERE
  5      unit_code = 'FIT9999';

1 row updated.

SQL> 
SQL> -- Task 2: Enter the mark and grade for the student with the student number of 11111113 
SQL> -- for the unit code FIT5132 that the student enrolled in semester 2 of 2014. The mark is 75 and the grade is D.
SQL> 
SQL> UPDATE enrolment
  2  SET
  3      enrol_mark = 75,
  4      enrol_grade = 'D'
  5  WHERE
  6      stu_nbr = 11111113
  7      AND unit_code = 'FIT5132'
  8      AND enrol_semester = 2
  9      AND enrol_year = 2014;

0 rows updated.

SQL> 
SQL> -- Task 3: The university introduced a new grade classification. The new classification are:
SQL> -- 1. 45-54isP1.
SQL> -- 2. 55-64isP2.
SQL> -- 3. 65-74isC.
SQL> -- 4. 75-84isD.
SQL> -- 5. 85-100isHD.
SQL> -- Change the database to reflect the new grade classification.
SQL> UPDATE enrolment
  2      SET enrol_grade = 'P1'
  3      WHERE enrol_mark BETWEEN 45 and 54;

0 rows updated.

SQL> 
SQL> UPDATE enrolment
  2      SET enrol_grade = 'P2'
  3      WHERE enrol_mark BETWEEN 55 AND 64;

1 row updated.

SQL> 
SQL> UPDATE enrolment
  2      SET enrol_grade = 'C'
  3      WHERE enrol_mark BETWEEN 65 AND 74;

0 rows updated.

SQL> 
SQL> UPDATE enrolment
  2      SET enrol_grade = 'D'
  3      WHERE enrol_mark BETWEEN 75 AND 84;

3 rows updated.

SQL> 
SQL> UPDATE enrolment
  2      SET enrol_grade = 'HD'
  3      WHERE enrol_mark BETWEEN 85 AND 100;

0 rows updated.

SQL> COMMIT;

Commit complete.

SQL> 
SQL> -- 9.2.2
SQL> 
SQL> -- Task 1: A student with student number 11111114 has taken intermission in semester 2 2014, 
SQL> -- hence all the enrolment of this student for semester 2 2014 should be removed. 
SQL> -- Change the database to reflect this situation.
SQL> DELETE FROM enrolment
  2  WHERE stu_nbr = 11111114 AND enrol_semester = 2 AND enrol_year = 2014;

0 rows deleted.

SQL> commit;

Commit complete.

SQL> 
SQL> -- Task 2: Assume that Wendy Wheat (student number 11111113) has withdrawn from the university. 
SQL> -- Remove her details from the database.
SQL> DELETE FROM enrolment
  2  WHERE stu_nbr = 11111113;

0 rows deleted.

SQL> -- integrity constraint (YLEE0026.ENROL_STU_FK) violated - child record found
SQL> DELETE FROM student
  2  WHERE stu_nbr = 11111113;

0 rows deleted.

SQL> COMMIT;

Commit complete.

SQL> 
SQL> -- Task 3: Add Wendy Wheat back to the database
SQL> INSERT INTO student VALUES (
  2      11111113,
  3      'Wheat',
  4      'Wendy',
  5      '05-May-1990'
  6  );

1 row inserted.

SQL> 
SQL> INSERT INTO enrolment (
  2      stu_nbr,
  3      unit_code,
  4      enrol_year,
  5      enrol_semester,
  6      enrol_mark,
  7      enrol_grade
  8  ) VALUES (
  9      11111113,
 10      'FIT5111',
 11      2014,
 12      '2',
 13      NULL,
 14      NULL
 15  );

1 row inserted.

SQL> INSERT INTO enrolment (
  2      stu_nbr,
  3      unit_code,
  4      enrol_year,
  5      enrol_semester,
  6      enrol_mark,
  7      enrol_grade
  8  ) VALUES (
  9      11111113,
 10      'FIT5132',
 11      2014,
 12      '2',
 13      NULL,
 14      NULL
 15  );

1 row inserted.

SQL> COMMIT;

Commit complete.

SQL> 
SQL> ALTER table enrolment DROP CONSTRAINT ENROL_STU_FK;

Table ENROLMENT altered.

SQL> ALTER table enrolment ADD CONSTRAINT ENROL_STU_FK foreign key (stu_nbr) references student (stu_nbr) on DELETE CASCADE;

Table ENROLMENT altered.

SQL> 
SQL> SELECT * FROM enrolment;
