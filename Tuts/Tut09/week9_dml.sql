-- Lee Yu Huei 29350336

SET ECHO ON
SPOOL week9_dml_output.txt
-- 9.2.1

-- Task 1: Update the unit name of FIT9999 from 'FIT Last Unit' to 'place holder unit'.
UPDATE unit
SET
    unit_name = 'place holder unit'
WHERE
    unit_code = 'FIT9999';

-- Task 2: Enter the mark and grade for the student with the student number of 11111113 
-- for the unit code FIT5132 that the student enrolled in semester 2 of 2014. The mark is 75 and the grade is D.

UPDATE enrolment
SET
    enrol_mark = 75,
    enrol_grade = 'D'
WHERE
    stu_nbr = 11111113
    AND unit_code = 'FIT5132'
    AND enrol_semester = 2
    AND enrol_year = 2014;

-- Task 3: The university introduced a new grade classification. The new classification are:
-- 1. 45-54isP1.
-- 2. 55-64isP2.
-- 3. 65-74isC.
-- 4. 75-84isD.
-- 5. 85-100isHD.
-- Change the database to reflect the new grade classification.
UPDATE enrolment
    SET enrol_grade = 'P1'
    WHERE enrol_mark BETWEEN 45 and 54;
    
UPDATE enrolment
    SET enrol_grade = 'P2'
    WHERE enrol_mark BETWEEN 55 AND 64;
    
UPDATE enrolment
    SET enrol_grade = 'C'
    WHERE enrol_mark BETWEEN 65 AND 74;
    
UPDATE enrolment
    SET enrol_grade = 'D'
    WHERE enrol_mark BETWEEN 75 AND 84;
    
UPDATE enrolment
    SET enrol_grade = 'HD'
    WHERE enrol_mark BETWEEN 85 AND 100;
COMMIT;
    
-- 9.2.2

-- Task 1: A student with student number 11111114 has taken intermission in semester 2 2014, 
-- hence all the enrolment of this student for semester 2 2014 should be removed. 
-- Change the database to reflect this situation.
DELETE FROM enrolment
WHERE stu_nbr = 11111114 AND enrol_semester = 2 AND enrol_year = 2014;
commit;

-- Task 2: Assume that Wendy Wheat (student number 11111113) has withdrawn from the university. 
-- Remove her details from the database.
DELETE FROM enrolment
WHERE stu_nbr = 11111113;
-- integrity constraint (YLEE0026.ENROL_STU_FK) violated - child record found
DELETE FROM student
WHERE stu_nbr = 11111113;
COMMIT;

-- Task 3: Add Wendy Wheat back to the database
INSERT INTO student VALUES (
    11111113,
    'Wheat',
    'Wendy',
    '05-May-1990'
);

INSERT INTO enrolment (
    stu_nbr,
    unit_code,
    enrol_year,
    enrol_semester,
    enrol_mark,
    enrol_grade
) VALUES (
    11111113,
    'FIT5111',
    2014,
    '2',
    NULL,
    NULL
);
INSERT INTO enrolment (
    stu_nbr,
    unit_code,
    enrol_year,
    enrol_semester,
    enrol_mark,
    enrol_grade
) VALUES (
    11111113,
    'FIT5132',
    2014,
    '2',
    NULL,
    NULL
);
COMMIT;

ALTER table enrolment DROP CONSTRAINT ENROL_STU_FK;
ALTER table enrolment ADD CONSTRAINT ENROL_STU_FK foreign key (stu_nbr) references student (stu_nbr) on DELETE CASCADE;

SELECT * FROM enrolment;

DELETE FROM student
WHERE stu_nbr = 11111113;

SELECT * FROM enrolment;

COMMIT;


SPOOL OFF
SET ECHO OFF
