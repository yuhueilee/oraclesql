--add set echo and spool command here
SET ECHO ON

SPOOL week10_sql_intermediate_output.txt
/*
Databases Week 10 Tutorial
week10_sql_intermediate.sql

student id: 29350336
student name: Lee Yu Huei
last modified date: 27/05/2020

*/

/* 1. Find the average mark of FIT1040 in semester 2, 2013. */

SELECT
    round(AVG(mark), 2) AS averagemark
FROM
    uni.enrolment
WHERE
    unitcode = 'FIT1040'
    AND semester = 2
    AND to_char(ofyear, 'yyyy') = '2013';

/* 2. List the average mark for each offering of FIT1040. 
In the listing, you need to include the year and semester number. 
Sort the result according to the year..*/

SELECT
    to_char(ofyear, 'yyyy') AS year,
    semester,
    round(AVG(mark), 2) AS averagemark
FROM
    uni.enrolment
WHERE
    unitcode = 'FIT1040'
GROUP BY
    to_char(ofyear, 'yyyy'),
    semester
ORDER BY
    to_char(ofyear, 'yyyy'),
    semester;

/* 3. Find the number of students enrolled in the unit FIT1040 in the year 2013, under the following conditions:
      a. Repeat students are counted each time
      b. Repeat students are only counted once
*/

SELECT
    COUNT(*) AS noofstud
FROM
    uni.enrolment
WHERE
    unitcode = 'FIT1040'
    AND to_char(ofyear, 'yyyy') = '2013';

SELECT
    COUNT(DISTINCT studid) AS noofstud
FROM
    uni.enrolment
WHERE
    unitcode = 'FIT1040'
    AND to_char(ofyear, 'yyyy') = '2013';

/* 4. Find the total number of prerequisite units for FIT2077. */

SELECT
    COUNT(p.has_prereq_of) AS noofprereq
FROM
    uni.unit     u
    JOIN uni.prereq   p
    ON u.unitcode = p.unitcode
WHERE
    u.unitcode = 'FIT2077';
-- Alternative way (simpler)

SELECT
    COUNT(has_prereq_of) AS noofprereq
FROM
    uni.prereq
WHERE
    unitcode = 'FIT2077';
  
/* 5. Find the total number of prerequisite units for each unit. 
In the list, include the unitcode for which the count is applicable.*/

SELECT
    u.unitcode,
    COUNT(p.has_prereq_of) AS noofprereq
FROM
    uni.unit     u
    JOIN uni.prereq   p
    ON u.unitcode = p.unitcode
GROUP BY
    u.unitcode
HAVING
    COUNT(p.has_prereq_of) IS NOT NULL
ORDER BY
    u.unitcode;
-- Alternative way (simpler)

SELECT
    unitcode,
    COUNT(has_prereq_of) AS "NUMBER OF PREREQUISITES"
FROM
    uni.prereq
GROUP BY
    unitcode
ORDER BY
    unitcode;

/* 6. For each prerequisite unit, calculate how many times it has been used as prerequisite. 
Include the name of the prerequisite unit in the listing .*/
    
SELECT
    u.unitcode,
    u.unitname,
    COUNT(u.unitcode) AS nooftime
FROM
    uni.prereq   p
    JOIN uni.unit     u
    ON p.has_prereq_of = u.unitcode
GROUP BY
    u.unitcode,
    u.unitname
ORDER BY
    u.unitcode,
    u.unitname;

/* 7. Find the unit with the highest number of enrolments in a given offering in the year 2013. */

SELECT
    unitcode,
    semester,
    COUNT(studid) AS noofenrolments
FROM
    uni.enrolment
WHERE
    to_char(ofyear, 'yyyy') = '2013'
GROUP BY
    unitcode,
    semester
HAVING
    COUNT(studid) IN (
        SELECT
            MAX(COUNT(studid))
        FROM
            uni.enrolment
        WHERE
            to_char(ofyear, 'yyyy') = '2013'
        GROUP BY
            unitcode,
            semester
    )
ORDER BY
    unitcode,
    semester;

/* 8. Find all students enrolled in FIT1004 in semester 1, 2013 
who have scored more than the average mark of FIT1004 in the same offering? 
Display the students' name and the mark. 
Sort the list in the order of the mark from the highest to the lowest.*/

SELECT
    s.studlname
    || ' '
    || s.studfname AS studname,
    e.mark
FROM
    uni.enrolment   e
    JOIN uni.student     s
    ON e.studid = s.studid
WHERE
    e.unitcode = 'FIT1004'
    AND e.semester = 1
    AND to_char(e.ofyear, 'yyyy') = '2013'
    AND e.mark > (
        SELECT
            AVG(mark)
        FROM
            uni.enrolment
        WHERE
            unitcode = 'FIT1004'
            AND semester = 1
            AND to_char(ofyear, 'yyyy') = '2013'
    )
ORDER BY
    e.mark DESC;

SPOOL OFF

SET ECHO OFF