-- 10.2.1 Lead Tutor Demo
SET ECHO ON

-- 1. Find the maximum mark for FIT1004 in semester 1, 2013.

SELECT
    MAX(mark)
FROM
    uni.enrolment
WHERE
    unitcode = 'FIT1004'
    AND semester = 1
    AND to_char(ofyear, 'YYYY') = '2013'; 

-- 2. Find the total number of enrolments per semester for each unit in the year 2013. 
-- The list should include the unitcode, semester and year. 
-- Order the list in increasing order of enrolment numbers.

SELECT
    unitcode,
    semester,
    to_char(ofyear, 'YYYY'),
    COUNT(studid) AS noofenrolment
FROM
    uni.enrolment
WHERE
    to_char(ofyear, 'YYYY') = '2013'
GROUP BY
    unitcode,
    semester,
    ofyear
ORDER BY
    COUNT(studid);

-- 3. Who is the oldest student in FIT1004? Display the studentís full name and the date of birth.
-- Wrong answer below since didn't select the unitcode which is FIT1004
SELECT
    studlname
    || ' '
    || studfname AS studname,
    to_char(studdob, 'dd/Mon/yyyy') AS dob
FROM
    uni.student
WHERE
    studdob IN (
        SELECT
            MIN(studdob)
        FROM
            uni.student
    );
-- Correst answer below
SELECT
    studfname
    || ' '
    || studlname AS fullname,
    to_char(studdob, 'dd/mm/yyyy') AS date_of_birth
FROM
    uni.student     s
    JOIN uni.enrolment   e
    ON s.studid = e.studid
WHERE
    e.unitcode = 'FIT1004'
    AND studdob = (
        SELECT
            MIN(studdob)
        FROM
            uni.student     s
            JOIN uni.enrolment   e
            ON s.studid = e.studid
        WHERE
            e.unitcode = 'FIT1004'
    )
ORDER BY
    s.studid;

SET ECHO OFF