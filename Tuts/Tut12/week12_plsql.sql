-- Lee Yu Huei 29350336
SET ECHO ON

SET SERVEROUTPUT ON;

SPOOL week12_plsql_output.txt

-- 12.4 FIT3171 Trigger Exercise

DROP TRIGGER unit_upd_cascade;

DROP TRIGGER maintain_stud_no;

DROP TRIGGER calculate_grade;

/*
1. Create a trigger unit_upd_cascade that updates matching rows in the ENROLMENT table 
whenever a unit code is updated in the UNIT table.
*/

CREATE OR REPLACE TRIGGER unit_upd_cascade AFTER
    UPDATE OF unit_code ON unit
    FOR EACH ROW
BEGIN
    UPDATE enrolment
    SET
        unit_code = :new.unit_code
    WHERE
        unit_code = :old.unit_code;

    dbms_output.put_line('Corresponding unit_code number in the ENROLMENT table has also been updated'
    );
END;
/ 

-- display before value

SELECT
    *
FROM
    unit;
-- test the trigger for insertion

UPDATE unit
SET
    unit_code = 'FIT9999'
WHERE
    unit_code = 'FIT9132';
-- display after value

SELECT
    *
FROM
    unit;

SELECT
    *
FROM
    enrolment;

ROLLBACK;

/*
2. Create a trigger that maintains the integrity of the total number of students in a given unit in the UNIT table
and record a delete operation in the audit_log.
*/

CREATE OR REPLACE TRIGGER maintain_stud_no AFTER
    INSERT OR DELETE ON enrolment
    FOR EACH ROW
BEGIN
    IF inserting THEN
    -- Update total number of students in unit table
        UPDATE unit
        SET
            unit_current_stu_count = unit_current_stu_count + 1
        WHERE
            unit_code = :new.unit_code;

        dbms_output.put_line('Total number of students for '
                             || :new.unit_code
                             || ' has been updated by incrementing 1');
    ELSE
        IF deleting THEN
        -- Update total number of students in unit table
            UPDATE unit
            SET
                unit_current_stu_count = unit_current_stu_count - 1
            WHERE
                unit_code = :old.unit_code;

            dbms_output.put_line('Total number of students for '
                                 || :old.unit_code
                                 || ' has been updated by decrementing 1');

        -- Insert row to audit_log table
            INSERT INTO audit_log VALUES (
                audit_seq.NEXTVAL,
                sysdate,
                user,
                :old.stu_no,
                :old.unit_code
            );

        END IF;
    END IF;
END;
/

-- Insert student into enrolment

SELECT
    *
FROM
    student;

SELECT
    *
FROM
    unit;

INSERT INTO student VALUES (
    '99999999',
    'Test',
    'Test',
    TO_DATE('01-JAN-90', 'DD-MON-RR'),
    NULL
);

INSERT INTO enrolment VALUES (
    '99999999',
    'FIT9132',
    2,
    2015,
    NULL,
    NULL
);

SELECT
    *
FROM
    student;

SELECT
    *
FROM
    unit;

-- Delete student from enrolment

SELECT
    *
FROM
    student;

SELECT
    *
FROM
    unit;

DELETE FROM enrolment
WHERE
    stu_no = '99999999'
    AND unit_code = 'FIT9132'
    AND enrol_semester = 2
    AND enrol_year = 2015;

SELECT
    *
FROM
    student;

SELECT
    *
FROM
    unit;
    
ROLLBACK;

/*
3. Create a trigger called calculate_grade that calculates the studentís grade (enrolment.enrol_grade) 
whenever a mark is updated for an enrolment or a new enrolment is added with a mark. 
*/

CREATE OR REPLACE TRIGGER calculate_grade AFTER
    INSERT OR UPDATE OF enrol_mark ON enrolment
    FOR EACH ROW
BEGIN
    :new.enrol_grade :=
        CASE
            WHEN :new.enrol_mark >= 80 THEN
                'HD'
            WHEN :new.enrol_mark BETWEEN 79 AND 70 THEN
                'D'
            WHEN :new.enrol_mark BETWEEN 69 AND 60 THEN
                'C'
            WHEN :new.enrol_mark BETWEEN 59 AND 50 THEN
                'P'
            ELSE 'N'
        END;
END;
/

CREATE OR REPLACE TRIGGER calculate_grade BEFORE
    INSERT OR UPDATE OF enrol_mark ON enrolment
    FOR EACH ROW
BEGIN
    :new.enrol_grade :=
        CASE
            WHEN :new.enrol_mark >= 80 THEN
                'HD'
            WHEN :new.enrol_mark >= 70 THEN
                'D'
            WHEN :new.enrol_mark >= 60 THEN
                'C'
            WHEN :new.enrol_mark >= 50 THEN
                'P'
            ELSE 'N'
        END;

    dbms_output.put_line('Mark for new enrolment has been updated');
END;
/

-- Update mark in enrolment

SELECT
    *
FROM
    enrolment;

UPDATE enrolment
SET
    enrol_mark = 70
WHERE
    stu_no = '11111121'
    AND unit_code = 'FIT9131'
    AND enrol_semester = 1
    AND enrol_year = 2014;

SELECT
    *
FROM
    enrolment;

ROLLBACK;

SPOOL OFF

SET ECHO OFF