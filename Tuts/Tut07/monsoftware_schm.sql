-- Drop any created tables
DROP TABLE employee CASCADE CONSTRAINTS PURGE;
DROP TABLE team CASCADE CONSTRAINTS PURGE;

SET ECHO ON; -- to show the result of running the statement
-- Create employee and team table
CREATE TABLE employee
(
	emp_no NUMBER(5) NOT NULL,
	emp_fname VARCHAR2(30),
	emp_lname VARCHAR2(30),
	emp_street VARCHAR2(50) NOT NULL,
	emp_town VARCHAR2(30) NOT NULL,
	emp_postcode CHAR(4) NOT NULL,
	emp_dob DATE NOT NULL,
	emp_taxno VARCHAR2(20),
	CONSTRAINT pk_emp_no PRIMARY KEY (emp_no)
);

CREATE TABLE team
(
	team_no NUMBER(3) NOT NULL,
	CONSTRAINT pk_team_no PRIMARY KEY (team_no)
);

-- Build the relaitonships
-- Each team can have 1 or many employees; each employee can be in 0 or 1 team
-- The pk of team is placed in the employee as its fk
-- Team is mandatory in this relationship
ALTER TABLE EMPLOYEE
	ADD(
    team_no NUMBER(3),
	CONSTRAINT fk_employee_team FOREIGN KEY (team_no)
		REFERENCES team (team_no) ON DELETE SET NULL
);

-- Each team is leaded by 0 or 1 employee; each employee can lead 0 or 1 team
-- The pk of employee is placed in the team as its fk
-- Both are optional in this relationship
ALTER TABLE team
	ADD
	(
        emp_no NUMBER(5),
		CONSTRAINT fk_team_employee FOREIGN KEY (emp_no)
			REFERENCES employee (emp_no) ON DELETE SET NULL
);

-- Each employee mentors 0 or many employees; each employee can be mentored by 0 or 1 employee
-- Both are optional in this relationship
-- The pk of the employee has placed in the employee as its foreign key
ALTER TABLE employee
	ADD(
        mentor_no NUMBER(5),
		CONSTRAINT fk_employee_employee FOREIGN KEY (mentor_no)
		REFERENCES employee (emp_no) ON DELETE SET NULL
);

COMMIT;
SET ECHO OFF;
