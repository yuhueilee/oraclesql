-- Lee Yu Huei 29350336

-- 7.2 Create table
SET ECHO ON

SPOOL week7_schema_output.txt

DROP TABLE enrolment PURGE;

DROP TABLE unit PURGE;

DROP TABLE student;

-- Create unit table

CREATE TABLE unit (
    unit_code   CHAR(7),
    unit_name   VARCHAR(50) NOT NULL UNIQUE
);
-- Set constraint

ALTER TABLE unit ADD (
    CONSTRAINT unit_pk PRIMARY KEY ( unit_code )
);

-- Create student table

CREATE TABLE student (
    stu_nbr     NUMBER(8),
    stu_fname   VARCHAR(50) NOT NULL,
    stu_lname   VARCHAR(50) NOT NULL,
    stu_dob     DATE NOT NULL
);
-- Set constraint

ALTER TABLE student ADD (
    CONSTRAINT student_pk PRIMARY KEY ( stu_nbr ),
    CONSTRAINT stu_nbr_value CHECK ( stu_nbr > 10000000 )
);

-- Create enrolment table

CREATE TABLE enrolment (
    enrol_year       NUMBER(4),
    enrol_semester   CHAR(1),
    enrol_mark       NUMBER(3),
    enrol_grade      CHAR(2),
    stu_nbr          NUMBER(8),
    unit_code        CHAR(7)
);
-- Set constraint

ALTER TABLE enrolment ADD (
    CONSTRAINT enrol_unit_fk FOREIGN KEY ( unit_code )
        REFERENCES unit ( unit_code ),
    CONSTRAINT enrol_stu_fk FOREIGN KEY ( stu_nbr )
        REFERENCES student ( stu_nbr ),
    CONSTRAINT enrol_pk PRIMARY KEY ( unit_code,
                                      stu_nbr,
                                      enrol_year,
                                      enrol_semester ),
    CONSTRAINT enrol_sem_value CHECK ( enrol_semester IN (
        '1',
        '2',
        '3'
    ) )
);

COMMIT;

SPOOL OFF

SET ECHO OFF

-- Check if the tables have been created sucessfully
SELECT table_name FROM user_tables;