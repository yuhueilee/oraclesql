-- Lee Yu Huei 29350336

-- 7.3
SET ECHO ON

SPOOL week7_insert_output.txt

--DELETE FROM enrolment;

--DELETE FROM unit;

--DELETE FROM student;

--DROP TABLE fit5111_student PURGE;

--DROP SEQUENCE student_seq;

-- 7.3.1 Insert statement

-- Insert data to unit table

INSERT INTO unit (
    unit_code,
    unit_name
) VALUES (
    'FIT9999',
    'FIT Last Unit'
);

INSERT INTO unit (
    unit_code,
    unit_name
) VALUES (
    'FIT5132',
    'Introduction to Databases'
);

INSERT INTO unit (
    unit_code,
    unit_name
) VALUES (
    'FIT5016',
    'Project'
);

INSERT INTO unit (
    unit_code,
    unit_name
) VALUES (
    'FIT5111',
    'Student''s Life'
);

COMMIT;

-- Insert data into student table

INSERT INTO student (
    stu_nbr,
    stu_lname,
    stu_fname,
    stu_dob
) VALUES (
    11111111,
    'Bloggs',
    'Fred',
    TO_DATE('1-Jan-90', 'dd-Mon-yy')
);

INSERT INTO student (
    stu_nbr,
    stu_lname,
    stu_fname,
    stu_dob
) VALUES (
    11111112,
    'Nice',
    'Nick',
    TO_DATE('10-Oct-94', 'dd-Mon-yy')
);

INSERT INTO student (
    stu_nbr,
    stu_lname,
    stu_fname,
    stu_dob
) VALUES (
    11111113,
    'Wheat',
    'Wendy',
    TO_DATE('5-May-90', 'dd-Mon-yy')
);

INSERT INTO student (
    stu_nbr,
    stu_lname,
    stu_fname,
    stu_dob
) VALUES (
    11111114,
    'Sheen',
    'Cindy',
    TO_DATE('25-Dec-96', 'dd-Mon-yy')
);

COMMIT;

-- Insert data into enrolment table

INSERT INTO enrolment (
    stu_nbr,
    unit_code,
    enrol_year,
    enrol_semester,
    enrol_mark,
    enrol_grade
) VALUES (
    11111111,
    'FIT5132',
    2013,
    '1',
    35,
    'N'
);

INSERT INTO enrolment (
    stu_nbr,
    unit_code,
    enrol_year,
    enrol_semester,
    enrol_mark,
    enrol_grade
) VALUES (
    11111111,
    'FIT5016',
    2013,
    '1',
    61,
    'C'
);

INSERT INTO enrolment (
    stu_nbr,
    unit_code,
    enrol_year,
    enrol_semester,
    enrol_mark,
    enrol_grade
) VALUES (
    11111111,
    'FIT5132',
    2013,
    '2',
    42,
    'N'
);

INSERT INTO enrolment (
    stu_nbr,
    unit_code,
    enrol_year,
    enrol_semester,
    enrol_mark,
    enrol_grade
) VALUES (
    11111111,
    'FIT5111',
    2013,
    '2',
    76,
    'D'
);

COMMIT;

INSERT INTO enrolment (
    stu_nbr,
    unit_code,
    enrol_year,
    enrol_semester,
    enrol_mark,
    enrol_grade
) VALUES (
    11111111,
    'FIT5132',
    2014,
    '2',
    NULL,
    NULL
);

COMMIT;

INSERT INTO enrolment (
    stu_nbr,
    unit_code,
    enrol_year,
    enrol_semester,
    enrol_mark,
    enrol_grade
) VALUES (
    11111112,
    'FIT5132',
    2013,
    '2',
    83,
    'HD'
);

INSERT INTO enrolment (
    stu_nbr,
    unit_code,
    enrol_year,
    enrol_semester,
    enrol_mark,
    enrol_grade
) VALUES (
    11111112,
    'FIT5111',
    2013,
    '2',
    79,
    'D'
);

COMMIT;

INSERT INTO enrolment (
    stu_nbr,
    unit_code,
    enrol_year,
    enrol_semester,
    enrol_mark,
    enrol_grade
) VALUES (
    11111113,
    'FIT5132',
    2014,
    '2',
    NULL,
    NULL
);

INSERT INTO enrolment (
    stu_nbr,
    unit_code,
    enrol_year,
    enrol_semester,
    enrol_mark,
    enrol_grade
) VALUES (
    11111113,
    'FIT5111',
    2014,
    '2',
    NULL,
    NULL
);

COMMIT;

INSERT INTO enrolment (
    stu_nbr,
    unit_code,
    enrol_year,
    enrol_semester,
    enrol_mark,
    enrol_grade
) VALUES (
    11111114,
    'FIT5111',
    2014,
    '2',
    NULL,
    NULL
);

COMMIT;

-- 7.3.2 Create sequence

-- Create a sequence for student table

CREATE SEQUENCE student_seq START WITH 11111115 INCREMENT BY 1;

-- Insert a new student usnig the sequence number

INSERT INTO student (
    stu_nbr,
    stu_lname,
    stu_fname,
    stu_dob
) VALUES (
    student_seq.NEXTVAL,
    'MICKEY',
    'MOUSE',
    TO_DATE('03-Jan-98', 'dd-Mon-yy')
);

COMMIT;

-- Insert enrolment for the new student

INSERT INTO enrolment (
    stu_nbr,
    unit_code,
    enrol_year,
    enrol_semester,
    enrol_mark,
    enrol_grade
) VALUES (
    student_seq.CURRVAL,
    'FIT5132',
    2016,
    '2',
    NULL,
    NULL
);

COMMIT;

-- 7.3.3 Advanced Insert

-- Create a new student

INSERT INTO student (
    stu_nbr,
    stu_lname,
    stu_fname,
    stu_dob
) VALUES (
    student_seq.NEXTVAL,
    'Lee',
    'Wendy',
    TO_DATE('13-Jul-98', 'dd-Mon-yy')
);

COMMIT;
-- Enrol the new student into "Introduction to databases"

INSERT INTO enrolment (
    stu_nbr,
    unit_code,
    enrol_year,
    enrol_semester,
    enrol_mark,
    enrol_grade
) VALUES (
    student_seq.CURRVAL,
    (
        SELECT
            unit_code
        FROM
            unit
        WHERE
            unit_name = 'Introduction to Databases'
    ),
    2016,
    '2',
    NULL,
    NULL
);

COMMIT; 

-- 7.3.4 Creating a table and inserting data as a single SQL statement

-- Create a table called FIT5111_STUDENT

CREATE TABLE FIT5111_student
    AS
        SELECT
            *
        FROM
            enrolment
        WHERE
            unit_code = 'FIT5111';

-- List the content of the table

SELECT
    *
FROM
    fit5111_student;

COMMIT;

SET ECHO OFF

SPOOL OFF