-- Lee Yu Huei 29350336

-- 7.4 Altering the structure of the table
SET ECHO ON

-- Add a new column in unit for credit point which the default is 6
ALTER TABLE unit
    ADD (unit_cred NUMBER(2, 0) DEFAULT(6));

-- Insert a new unit
INSERT INTO unit (unit_code, unit_name, unit_cred) VALUES ('FIT3171', 'DB', 6);

COMMIT;

SET ECHO OFF
