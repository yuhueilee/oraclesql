--add set echo and spool command here
SET ECHO ON
SPOOL week8_SQLBasic_PartB_output.txt
/*
Databases Week 8 Tutorial Sample Solution
week8_SQLBasic_PartB.sql

student id: 29350336
student name: Lee Yu Huei
last modified date: 13/05/2020
*/

/* B1. List all the unit codes, semester and name of the chief examiner for all 
      the units that are offered in 2014.*/
SELECT o.unitcode, o.semester, s.stafffname || ' ' || s.stafflname AS name
FROM uni.offering o JOIN uni.staff s ON o.chiefexam = s.staffid
WHERE to_char(o.ofyear, 'yyyy') = '2014'
ORDER BY o.unitcode;

  

/* B2. List all the unit codes and the unit names and their year and semester 
      offerings. To display the date correctly in Oracle, you need to use to_char function. 
      For example, to_char(ofyear,'YYYY'). */
SELECT o.unitcode, u.unitname, to_char(o.ofyear, 'yyyy'), o.semester
FROM uni.offering o JOIN uni.unit u ON o.unitcode = u.unitcode;
  

/* B3. List the unit code, semester, class type (lecture or tutorial), 
      day and time for all units taught by Albus Dumbledore in 2013. 
      Sort the list according to the unit code.*/
SELECT sc.unitcode, sc.semester, sc.cltype, sc.clday, to_char(sc.cltime, 'HH:MM') AS cltime
FROM uni.schedclass sc JOIN uni.staff s ON sc.staffid = s.staffid
WHERE s.stafffname = 'Albus' AND s.stafflname = 'Dumbledore' AND to_char(sc.ofyear, 'yyyy') = '2013'
ORDER BY sc.unitcode;


/* B4. Create a study statement for Mary Smith. A study statement contains 
      unit code, unit name, semester and year study was attempted, the mark 
      and grade. */
SELECT e.unitcode, u.unitname, e.semester, to_char(e.ofyear, 'yyyy') AS ofyear, e.mark, e.grade
FROM uni.enrolment e JOIN uni.unit u ON e.unitcode = u.unitcode
WHERE e.studid = (SELECT studid FROM uni.student WHERE studfname = 'Mary' AND studlname = 'Smith');


/* B5. List the unit code and unit name of the pre-requisite units of 
      'Advanced Data Management' unit */
SELECT p.has_prereq_of, u1.unitname
FROM uni.unit u JOIN uni.prereq p ON u.unitcode = p.unitcode
JOIN uni.unit u1 ON p.has_prereq_of = u1.unitcode
WHERE u.unitname = 'Advanced Data Management';


/* B6. Find all students (list their id, firstname and surname) who 
       have a failed unit in the year 2013 */
SELECT s.studid, s.studfname, s.studlname
FROM uni.student s JOIN uni.enrolment e ON s.studid = e.studid
WHERE e.grade = 'N' AND to_char(e.ofyear, 'yyyy') = '2013';


/* B7.	List the student name, unit code, semester and year for those 
        students who do not have marks recorded */
SELECT s.studfname || ' ' || s.studlname AS name, e.unitcode, e.semester, to_char(e.ofyear, 'yyyy') AS ofyear
FROM uni.student s JOIN uni.enrolment e ON s.studid = e.studid
WHERE e.mark IS NULL;


SPOOL OFF
SET ECHO OFF
