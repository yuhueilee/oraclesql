-- Lee Yu Huei 29350336
-- 05/05/2020

-- Workshop questions

-- Q1. Show the ids, names of students as a single column called NAME and their DOBs. 
-- Order the output in date of birth order
SELECT
    studid,
    studfname
    || ' '
    || studlname AS "NAME",
    to_char(studdob, 'dd/Mon/yyyy') AS "DOB"
FROM
    uni.student
ORDER BY
    studdob;

-- Q2. Show the ids, names of students as a single column called NAME, unit code, 
-- and year and semester of enrolment where the mark is NULL. Order the output by student id, within unit code order
SELECT
    studid,
    studfname
    || ' '
    || studlname AS "NAME",
    unitcode,
    to_char(ofyear, 'yyyy'),
    semester
FROM
    uni.student
    NATURAL JOIN uni.enrolment
WHERE
    mark IS NULL
ORDER BY
    unitcode,
    studid;