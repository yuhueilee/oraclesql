--add set echo and spool command here
SET ECHO ON
SPOOL week8_SQLBasic_PartA_output.txt
/*
Databases Week 8 Tutorial
week8_SQLBasic_PartA.sql

student id: 29350336
student name: Lee Yu Huei
last modified date: 13/05/2020

*/

/* A1. List ALL units AND their details. */
SELECT *
FROM uni.unit;


/* A2. List all students' details who have the surname 'Smith'.*/
SELECT *
FROM uni.student
WHERE studlname = 'Smith';


/* A3. List the student's surname, firstname and address for those students 
who have a surname starting with the letter 'S' and firstname contains the letter 'i'*/
SELECT studlname, studfname, studaddress
FROM uni.student
WHERE studlname LIKE 'S%' AND studfname LIKE '%i%';


/* A4. List the unit code of all units that are offered in semester 1 of 2014. */
SELECT unitcode
FROM uni.offering
WHERE semester = 1 AND to_char(ofyear, 'yyyy') = '2014';
-- WHERE semester = 1 AND ofyear = to_date('2014', 'yyyy'); -- cannot use this statement
  
  
/* A5. Assuming that a unit code is created based on the following rules:
a. The first three letters represent faculty abbreviation, eg FIT for the Faculty of Information Technology.
b. The first digit of the number following the letter represents the year level. 
   For example, FIT2094 is a unit code from Faculty of IT (FIT) and the number 2 refers to a second year unit.

List the unit details of all first year units in the Faculty of Information Technology.*/
SELECT *
FROM uni.unit
WHERE unitcode LIKE 'FIT1%';


/* A6. List the unit code and semester of all units that were offered in either semester 1 or summer of 2013.*/
SELECT unitcode, semester
FROM uni.offering
WHERE semester IN (1, 3) AND to_char(ofyear, 'yyyy') = '2013';


/* A7. List the student number, mark, unit code and semester for those tudents who have passed any unit in semester 1 of 2013. */
SELECT studid, mark, unitcode, semester
FROM uni.enrolment
WHERE mark > 50 AND semester = 1 AND to_char(ofyear, 'yyyy') = '2013';

SPOOL OFF
SET ECHO OFF
