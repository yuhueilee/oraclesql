--add set echo and spool command here

/*
Databases Week 11 Tutorial
week11_sql_advanced.sql

student id: 29350336
student name: Lee Yu Huei
last modified date: 03/06/2020

*/
SET ECHO ON

SPOOL week11_sql_advanced_output.txt
/* 1. Find the number of scheduled classes assigned to each staff member each year and semester. 
If the number of classes is 2 then this should be labelled as a correct load, 
more than 2 as an overload and less than 2 as an underload. 
Order the list by decreasing order of scheduled class number. */

SELECT
    to_char(c.ofyear, 'yyyy') AS year,
    c.semester,
    s.staffid,
    s.stafffname
    || ' '
    || s.stafflname AS staffname,
    COUNT(c.staffid) AS numberclasses,
    CASE
        WHEN COUNT(c.staffid) = 2 THEN
            'Correct load'
        WHEN COUNT(c.staffid) < 2 THEN
            'Underload'
        WHEN COUNT(c.staffid) > 2 THEN
            'Overload'
    END AS load
FROM
    uni.schedclass   c
    JOIN uni.staff        s
    ON c.staffid = s.staffid
GROUP BY
    c.ofyear,
    c.semester,
    s.staffid,
    s.stafffname,
    s.stafflname
ORDER BY
    COUNT(c.staffid) DESC;

/* 2. Display unit code and unitname for units that do not have a prerequisite. 
Order the list in increasing order of unit code.

There are many approaches that you can take in writing an SQL statement to answer this query. 
You can use the SET OPERATORS, OUTER JOIN and a SUBQUERY. 
Write SQL statements based on all of these approaches.*/
-- Approach 1: set operators

SELECT
    u.unitcode,
    u.unitname
FROM
    uni.unit u
MINUS
SELECT DISTINCT
    p.unitcode,
    u.unitname
FROM
    uni.prereq   p
    JOIN uni.unit     u
    ON p.unitcode = u.unitcode
ORDER BY
    unitcode;

-- Approach 2: outer join

SELECT
    u.unitcode,
    u.unitname
FROM
    uni.unit     u
    LEFT OUTER JOIN uni.prereq   p
    ON u.unitcode = p.unitcode
WHERE
    p.unitcode IS NULL
ORDER BY
    u.unitcode;

-- Approach 3: subquery

SELECT
    u.unitcode,
    u.unitname
FROM
    uni.unit u
WHERE
    u.unitcode NOT IN (
        SELECT
            p.unitcode
        FROM
            uni.prereq p
    )
ORDER BY
    u.unitcode;

/* 3. List all units offered in 2013 semester 2 which do not have any scheduled class. 
Include unit code, unit name, and chief examiner name in the list. 
Order the list based on the unit code. */

SELECT
    o.unitcode,
    u.unitname,
    s.stafffname
    || ' '
    || s.stafflname AS ce_name
FROM
    uni.staff      s
    JOIN uni.offering   o
    ON o.chiefexam = s.staffid
    JOIN uni.unit       u
    ON o.unitcode = u.unitcode
WHERE
    to_char(o.ofyear, 'yyyy') = '2013'
    AND o.semester = 2
    AND o.unitcode IN (
        SELECT
            o.unitcode
        FROM
            uni.schedclass   c
            RIGHT OUTER JOIN uni.offering     o
            ON c.unitcode = o.unitcode
               AND c.ofyear = o.ofyear
               AND c.semester = o.semester
        WHERE
            c.unitcode IS NULL
            AND to_char(o.ofyear, 'yyyy') = '2013'
            AND o.semester = 2
    )
ORDER BY
    o.unitcode;
    
-- Approach 2

SELECT
    o.unitcode,
    u.unitname,
    s.stafffname
    || ' '
    || s.stafflname AS "CHIEF EXAMINER"
FROM
    ( uni.schedclass   c
    RIGHT JOIN uni.offering     o
    ON c.unitcode = o.unitcode
       AND c.semester = o.semester
       AND c.ofyear = o.ofyear )
    JOIN uni.unit         u
    ON o.unitcode = u.unitcode
    JOIN uni.staff        s
    ON o.chiefexam = s.staffid
WHERE
    to_char(o.ofyear, 'yyyy') = '2013'
    AND o.semester = 2
    AND c.classno IS NULL
ORDER BY
    o.unitcode;

 
/* 4. List full names of students who are enrolled in both Introduction to Databases and Programming Foundations 
(note: unit names are unique). Order the list by student full name.*/

SELECT
    s.studfname
    || ' '
    || s.studlname AS stud_full_name
FROM
    uni.student     s
    JOIN uni.enrolment   e
    ON s.studid = e.studid
WHERE
    e.unitcode = (
        SELECT
            u.unitcode
        FROM
            uni.unit        u
            JOIN uni.enrolment   e
            ON u.unitcode = e.unitcode
        WHERE
            u.unitname = 'Introduction to Databases'
    )
INTERSECT
SELECT
    s.studfname
    || ' '
    || s.studlname AS stud_full_name
FROM
    uni.student     s
    JOIN uni.enrolment   e
    ON s.studid = e.studid
WHERE
    e.unitcode = (
        SELECT
            u.unitcode
        FROM
            uni.unit        u
            JOIN uni.enrolment   e
            ON u.unitcode = e.unitcode
        WHERE
            u.unitname = 'Programming Foundations'
    )
ORDER BY
    stud_full_name;
    
-- Approach 2

SELECT
    s.studfname
    || ' '
    || s.studlname AS name
FROM
    uni.student s
WHERE
    s.studid IN (
        SELECT
            studid
        FROM
            uni.enrolment   e
            JOIN uni.unit        u
            ON e.unitcode = u.unitcode
        WHERE
            u.unitname = 'Introduction to Databases'
        INTERSECT
        SELECT
            studid
        FROM
            uni.enrolment   e
            JOIN uni.unit        u
            ON e.unitcode = u.unitcode
        WHERE
            u.unitname = 'Programming Foundations'
    )
ORDER BY
    name ASC;

/* 5. Given that payment rate for tutorial is $42.85 per hour and  payemnt rate for lecture is $75.60 per hour, 
calculate weekly payment per type of class for each staff. 
In the display, include staff name, type of class (lecture or tutorial), 
number of classes, number of hours (total duration), and weekly payment (number of hours * payment rate). 
Order the list by increasing order of staff name*/

SELECT
    s.stafffname
    || ' '
    || s.stafflname AS staffname,
    CASE c.cltype
        WHEN 'L'   THEN
            'Lecture'
        WHEN 'T'   THEN
            'Tutorial'
    END AS type,
    COUNT(c.staffid) AS no_of_class,
    SUM(c.clduration) AS total_hours,
    CASE c.cltype
        WHEN 'L'   THEN
            to_char(SUM(c.clduration) * 75.6, '$990.00')
        WHEN 'T'   THEN
            to_char(SUM(c.clduration) * 42.85, '$990.00')
    END AS weekly_payment
FROM
    uni.staff        s
    JOIN uni.schedclass   c
    ON s.staffid = c.staffid
GROUP BY
    s.stafffname,
    s.stafflname,
    c.cltype
ORDER BY
    s.stafffname,
    s.stafflname;

/* 6. Assume that all units worth 6 credit points each, calculate each student’s Weighted Average Mark (WAM) and GPA. 
Please refer to these Monash websites: https://www.monash.edu/exams/results/wam and https://www.monash.edu/exams/results/gpa 
for more information about WAM and GPA respectively. 

Calculation example for student 111111111:
WAM = (65x3 + 45x3 + 74x3 +74*6)/(3+3+3+6) = 66.4
GPA = (2x6 + 0.3x6 + 3x6 + 3x6)/(6+6+6+6) = 2.08

Include student id, student full name (in a 40 character wide column headed “Student Full Name�?), WAM and GPA in the display. 
Order the list by descending order of WAM then descending order of GPA.
*/

SELECT
    studid,
    rpad("Student Full Name", 40, ' '),
    to_char(SUM((mark * credit_point)) / SUM(credit_point), '990.99') AS wam,
    to_char((SUM(gpa_grade_value) * 6) /(COUNT(studid) * 6), '990.99') AS gpa
FROM
    (
        SELECT
            s.studid,
            s.studfname
            || ' '
            || s.studlname AS "Student Full Name",
            e.unitcode,
            e.mark,
            CASE
                WHEN e.unitcode LIKE ( 'FIT1%' ) THEN
                    3
                ELSE
                    6
            END AS credit_point,
            CASE e.grade
                WHEN 'HD'   THEN
                    4
                WHEN 'D'    THEN
                    3
                WHEN 'C'    THEN
                    2
                WHEN 'P'    THEN
                    1
                WHEN 'N'    THEN
                    0.3
            END AS gpa_grade_value
        FROM
            uni.student     s
            LEFT OUTER JOIN uni.enrolment   e
            ON s.studid = e.studid
        ORDER BY
            s.studid
    )
GROUP BY
    studid,
    "Student Full Name"
ORDER BY
    wam DESC,
    gpa DESC;
    
-- Approach 2

SELECT
    s.studid,
    s.studfname
    || ' '
    || s.studlname AS student_fullname,
    to_char(SUM(
        CASE
            WHEN e.unitcode LIKE('FIT1%') THEN
                e.mark * 3
            ELSE
                e.mark * 6
        END
    ) / SUM(
        CASE
            WHEN e.unitcode LIKE('FIT1%') THEN
                3
            ELSE
                6
        END
    ), '990.99') AS wam,
    to_char((SUM(
        CASE e.grade
            WHEN 'F'    THEN
                0.3
            WHEN 'NP'   THEN
                0.7
            WHEN 'P'    THEN
                1
            WHEN 'C'    THEN
                2
            WHEN 'D'    THEN
                3
            WHEN 'HD'   THEN
                4
        END
    ) * 6) /(COUNT(e.mark) * 6), '990.99') AS gpa
FROM
    uni.student     s
    LEFT JOIN uni.enrolment   e
    ON s.studid = e.studid
GROUP BY
    s.studid,
    s.studfname
    || ' '
    || s.studlname
ORDER BY
    wam DESC,
    gpa DESC;

SPOOL OFF

SET ECHO OFF