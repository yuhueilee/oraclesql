-- 11.1 Lead Tutor Demo

/*
1. Assuming that student name is unique, display Rani Dewa�s academic record, 
include unitcode, unitname, year, semester, mark and explained_grade in the listing. 
Explain grade shows Fail for N, Pass for P, Credit for C, Distinction for D and High Distinction for HD. 
Order the list in increasing order of year, within the same year order the list in increasing order of semester.
*/
SELECT
    e.unitcode,
    u.unitname,
    to_char(e.ofyear, 'yyyy') AS year,
    e.semester,
    e.mark,
    CASE e.grade
        WHEN 'N'    THEN
            'Fail'
        WHEN 'P'    THEN
            'Pass'
        WHEN 'C'    THEN
            'Credit'
        WHEN 'D'    THEN
            'Distinction'
        WHEN 'HD'   THEN
            'High Distinction'
    END AS explained_grade
FROM
    uni.student     s
    JOIN uni.enrolment   e
    ON s.studid = e.studid
    JOIN uni.offering    o
    ON e.unitcode = o.unitcode
       AND e.semester = o.semester
       AND e.ofyear = o.ofyear
    JOIN uni.unit        u
    ON o.unitcode = u.unitcode
WHERE
    s.studfname = 'Rani'
    AND s.studlname = 'Dewa'
ORDER BY
    e.ofyear,
    e.semester;

-- Solution

SELECT
    unitcode,
    unitname,
    to_char(ofyear, 'yyyy') AS year,
    semester,
    mark,
    CASE grade
        WHEN 'N'    THEN
            'Fail'
        WHEN 'P'    THEN
            'Pass'
        WHEN 'C'    THEN
            'Credit'
        WHEN 'D'    THEN
            'Distinction'
        WHEN 'HD'   THEN
            'High Distinction'
    END AS explained_grade
FROM
    uni.enrolment
    NATURAL JOIN uni.unit
WHERE
    studid = (
        SELECT
            studid
        FROM
            uni.student
        WHERE
            upper(studfname) = upper('Rani')
            AND upper(studlname) = upper('Dewa')
    )
ORDER BY
    year,
    semester;

/*
2. Find the total number of prerequisite units for each unit. 
Include in the list the unit code of units that do not have a prerequisite. 
Order the list in descending order of the number of prerequisite units.
*/

SELECT
    u.unitcode,
    COUNT(p.unitcode) AS no_of_prereq
FROM
    uni.unit     u
    LEFT OUTER JOIN uni.prereq   p
    ON u.unitcode = p.unitcode
GROUP BY
    u.unitcode
ORDER BY
    COUNT(p.unitcode) DESC;
    
-- Solution

SELECT
    u.unitcode,
    COUNT(has_prereq_of) AS no_of_prereq
FROM
    uni.unit     u
    LEFT OUTER JOIN uni.prereq   p
    ON u.unitcode = p.unitcode
GROUP BY
    u.unitcode
ORDER BY
    no_of_prereq DESC;

/*
3. List the unitcode, year, semester, number of enrolments and the average mark for each offering. 
Include offerings without any enrolment in the list. 
Round the average to 2 digits after the decimal points. 
If the average result is 'null', display the average as 0.00. 
All values must be shown with two decimal digits. 
Order the list in increasing order of average mark.
*/
-- should place the nvl function after avg as avg will divide the total mark by the number of rows that has no null mark value
SELECT
    o.unitcode,
    to_char(o.ofyear, 'yyyy') AS year,
    o.semester,
    COUNT(e.studid) AS no_of_enrolment,
    to_char(nvl(round(AVG(e.mark), 2), 0), '990.99') AS average
FROM
    uni.enrolment   e
    RIGHT OUTER JOIN uni.offering    o
    ON e.unitcode = o.unitcode
       AND e.ofyear = o.ofyear
       AND e.semester = o.semester
GROUP BY
    o.unitcode,
    o.ofyear,
    o.semester
ORDER BY
    average;
    
-- Solution