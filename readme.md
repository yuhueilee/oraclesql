# Oracle SQL


- Create ER diagram based on business rules
- Implement a logical model in Oracle
- Generate a SQL script from the model
- Use SQL for querying